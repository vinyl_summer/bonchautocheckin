FROM python:3.9.7-buster
RUN apt-get update && apt-get install -y firefox-esr
RUN cd /usr/bin && wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz && tar xvf geckodriver-v0.30.0-linux64.tar.gz
ENV PYTHONUNBUFFERED=1
RUN mkdir /app
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app
CMD python main.py
