# BonchAutoCheckIn
This script automatically checks in a group of students at bonch account page

## Usage

### Manual
Set the following environment variables:
```shell
export BONCH_LOGINS=<A comma-separated list of student logins>
export BONCH_PASSWORDS=<A comma-separated list of student passwords>
```
Start the script:
```shell
python main.py
```

### Docker
After building the image, run it with the same environment variables set:
```shell
docker build -t bonchautocheckin .
docker run -d --env=BONCH_LOGINS=<Student logins> --env=BONCH_LOGINS=BONCH_PASSWORDS=<Student passwords> bonchautocheckin
```
