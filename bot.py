import os
import logging
import re

import telegram.ext
import telegram

import students

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import UnexpectedAlertPresentException
from peewee import IntegrityError
from peewee import DoesNotExist


def start_bot():
    assert "API_KEY" in os.environ

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)
    updater = telegram.ext.Updater(token=os.environ["API_KEY"])
    dispatcher = updater.dispatcher

    def start(update: telegram.Update, context: telegram.ext.CallbackContext):
        print('[СООБЩЕНИЕ] ' + update.effective_user.full_name + ': ' + update.message.text)
        context.bot.send_message(chat_id=update.effective_chat.id, text="Привет! Чтобы бот мог тебя отмечать, "
                                                                        "отправь данные о себе по образцу "
                                                                        "/register <группа> <логин> <пароль>\nНапример: "
                                                                        "/register ПСТ-78 ilovebonch727@mail.ru "
                                                                        "fakeDINS\nЕсли нужно, "
                                                                        "пароль можно поменять в личном "
                                                                        "кабинете")

    start_handler = telegram.ext.CommandHandler('start', start)
    dispatcher.add_handler(start_handler)

    def help(update: telegram.Update, context: telegram.ext.CallbackContext):
        print('[СООБЩЕНИЕ] ' + update.effective_user.full_name + ': ' + update.message.text)
        context.bot.send_message(chat_id=update.effective_chat.id, text='Вопросы и предложения можно '
                                                                        'писать @obama_xvatit')

    help_handler = telegram.ext.CommandHandler('help', help)
    dispatcher.add_handler(help_handler)

    def donate(update: telegram.Update, context: telegram.ext.CallbackContext):
        print('[СООБЩЕНИЕ] ' + update.effective_user.full_name + ': ' + update.message.text)
        context.bot.send_message(chat_id=update.effective_chat.id, text='Если есть желание, мою работу '
                                                                        'можно поддержать финансово!\nСбер: '
                                                                        '9117202793')

    donate_handler = telegram.ext.CommandHandler('donate', donate)
    dispatcher.add_handler(donate_handler)

    def student_verification(login, password):
        options = Options()
        options.headless = True

        driver = webdriver.Firefox(options=options)
        driver.get("https://lk.sut.ru/cabinet/?login=yes")
        assert 'bonch' in driver.title

        login_field = driver.find_element_by_id("users")
        password_field = driver.find_element_by_id("parole")
        log_button = driver.find_element_by_id("logButton")

        login_field.send_keys(login)
        password_field.send_keys(password)
        log_button.click()

        try:
            driver.find_element_by_id('login')
        except UnexpectedAlertPresentException:
            return False
        return True

    def email_is_valid(email):
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        if re.fullmatch(regex, email):
            return True
        else:
            return False

    def group_is_valid(group):
        regex = r'[а-яА-Я]+-[0-9]+'
        if re.fullmatch(regex, group):
            return True
        else:
            return False

    def add_student_to_db(login, password, group, telegram_id):
        new_student = students.Student.create(group=group.upper(), email=login, password=password,
                                              telegram_user_id=telegram_id)
        new_student.save()
        print('[УСПЕШНО] Ученик ' + login + ' из группы ' + group + 'был добавлен в базу данных')

    def register(update: telegram.Update, context: telegram.ext.CallbackContext):
        msg = update.message.text.split(" ")
        print('[СООБЩЕНИЕ] ' + update.effective_user.full_name + ': ' + update.message.text)

        if len(msg) != 4:
            context.bot.send_message(chat_id=update.effective_chat.id, text="Неверное использование команды!\nПример: "
                                                                            "/register ПСТ-78 ilovebonch727@mail.ru "
                                                                            "fakeDINS")
            print('[ОШИБКА] Неверно введена команда')
            return

        student_group = msg[1]
        student_login = msg[2]
        student_password = msg[3]

        if not group_is_valid(student_group):
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text="Неверно введено название группы!\nПример: "
                                          "/register ПСТ-78 ilovebonch727@mail.ru "
                                          "fakeDINS")
            print('[ОШИБКА]Неверное название группы')
            return

        if not email_is_valid(student_login):
            context.bot.send_message(chat_id=update.effective_chat.id, text="Неверно введён логин!\nПример: "
                                                                            "/register ПСТ-78 ilovebonch727@mail.ru "
                                                                            "fakeDINS")
            print('[ОШИБКА] Неверный логин')
            return

        context.bot.send_message(chat_id=update.effective_chat.id, text=f"Круто! Твоя группа: {student_group},"
                                                                        f" твой логин: {student_login},"
                                                                        f" твой пароль: {student_password}, "
                                                                        f" произвожу проверку введённых данных")
        print('[УСПЕШНО] Введены корректные данные')

        if student_verification(student_login, student_password):
            try:
                add_student_to_db(student_login, student_password, student_group, update.effective_user.id)
                context.bot.send_message(chat_id=update.effective_chat.id, text="Ты успешно зарегистрирован!")
            except IntegrityError:
                context.bot.send_message(chat_id=update.effective_chat.id,
                                         text="Пользователь с таким логином уже имеется")
        else:
            context.bot.send_message(chat_id=update.effective_chat.id, text="Студента с такими данными не существует")
            print('[ОШИБКА] Студента с такими данными не сущестует')

    register_handler = telegram.ext.CommandHandler('register', register)
    dispatcher.add_handler(register_handler)

    def toggle_check_in_property(update: telegram.Update, context: telegram.ext.CallbackContext):
        print('[СООБЩЕНИЕ] ' + update.effective_user.full_name + ': ' + update.message.text)
        try:
            student = students.Student.get(students.Student.telegram_user_id == update.effective_chat.id)
            if student.to_check_in:
                context.bot.send_message(chat_id=update.effective_chat.id, text="Бот больше не будет вас отмечать")
            else:
                context.bot.send_message(chat_id=update.effective_chat.id, text="Бот снова будет вас отмечать")
            student.to_check_in = not student.to_check_in
            student.save()
            print('[УСПЕШНО] ' + student.email + ' изменил значение графы to_check_in')
        except DoesNotExist:
            context.bot.send_message(chat_id=update.effective_chat.id, text="Ты не зарегистрирован!")
            print('[ОШИБКА] ' + update.effective_user.full_name + ' занимается чем-то не тем')

    toggle_handler = telegram.ext.CommandHandler('toggle', toggle_check_in_property)
    dispatcher.add_handler(toggle_handler)

    def get_last_check_in(update: telegram.Update, context: telegram.ext.CallbackContext):
        print('[СООБЩЕНИЕ] ' + update.effective_user.full_name + ': ' + update.message.text)
        try:
            student = students.Student.get(students.Student.telegram_user_id == update.effective_chat.id)
            last_check_in = student.last_check_in
            if last_check_in is not None:
                context.bot.send_message(chat_id=update.effective_chat.id, text="В последний раз ты"
                                                                                " был отмечен " + str(last_check_in))
            else:
                context.bot.send_message(chat_id=update.effective_chat.id, text="Тебя ещё ни разу не отмечало")
        except DoesNotExist:
            context.bot.send_message(chat_id=update.effective_chat.id, text="Ты не зарегистрирован!")
            print('[ОШИБКА] ' + update.effective_user.full_name + ' занимается чем-то не тем')

    get_last_check_in_handler = telegram.ext.CommandHandler('last', get_last_check_in)
    dispatcher.add_handler(get_last_check_in_handler)

    def listen(update, context):
        print('[СООБЩЕНИЕ] ' + update.effective_user.full_name + ': ' + update.message.text)

    listen_handler = telegram.ext.MessageHandler(telegram.ext.Filters.text & (~telegram.ext.Filters.command), listen)
    dispatcher.add_handler(listen_handler)

    updater.start_polling()
