import pytz
from datetime import datetime, timezone
import time

import bot
import students

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options


def check_in():
    groups = []
    query = students.Student.select().group_by(students.Student.group)
    for student in query:
        groups.append(student.group)

    for group in groups:
        first_student_log_in = False
        for student in students.Student.select().where(students.Student.group == group):
            if not student.to_check_in:
                continue

            options = Options()
            options.headless = True

            print(f"STARTING WEBDRIVER, logging in for {student.email} with password {student.password}")
            driver = webdriver.Firefox(options=options)
            driver.get("https://lk.sut.ru/cabinet/?login=yes")
            if "bonch" not in driver.title:
                print('bonch died')
                break

            login_field = driver.find_element_by_id("users")
            password_field = driver.find_element_by_id("parole")
            log_button = driver.find_element_by_id("logButton")
            print('Logging in ')

            student_login = student.email
            student_password = student.password
            login_field.send_keys(student_login)
            password_field.send_keys(student_password)
            log_button.click()

            print('Opening the website menu ')
            heading_learning = WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.TAG_NAME, "h5")))
            heading_learning.click()

            print("Opening today's schedule ")
            schedule = WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.ID, "menu_li_6118")))
            schedule.click()

            print('Searching for start lesson button ')
            WebDriverWait(driver, 30).until(EC.presence_of_all_elements_located((By.CLASS_NAME, 'simple-little-table')))
            start_lesson_list = driver.find_elements_by_xpath("//a[text() = 'Начать занятие']")
            if len(start_lesson_list) > 0:
                for element in start_lesson_list:
                    element.click()
                    print('Success!')

                    student.last_check_in = datetime.utcnow().replace(tzinfo=timezone.utc).astimezone(tz=None).strftime('%Y-%m-%d %H:%M')
                    student.save()

                    first_student_log_in = True
                    driver.quit()
                driver.quit()
            elif not first_student_log_in:
                print('Element not found ')
                driver.quit()
                break
            else:
                print(f"{student.email} has checked in by himself, what a dick")
                driver.quit()


if __name__ == '__main__':
    bot.start_bot()
    while True:
        check_in()
        time.sleep(600)
