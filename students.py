from peewee import *

db = SqliteDatabase('./database/students.db', timeout=30)


class Student(Model):
    email = CharField(unique=True, null=False)
    password = CharField(null=False)
    group = CharField(null=False)
    to_check_in = BooleanField(default=True)
    telegram_user_id = IntegerField(unique=True, null=False)
    last_check_in = DateTimeField(default=None)

    class Meta:
        database = db
        table_name = 'students'


db.connect()
db.create_tables([Student])
